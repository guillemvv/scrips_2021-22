
#! /bin/bash 
# @Guillem Vallespinos
# Curs 2021-2022
# febrer 2022
# Descripcio: exemples bucle while 
# ---------------------------------

# 7) numerar stdin linea linea i passar a majuscules
num=1
while read -r line
do
  echo "$num: $line " | tr 'a-z' 'A-Z'
  ((num++))
done
exit 0

# 6) processar stdin fins al token FI
read -r line
while [ "$line" != "FI" ]
do
  echo "$line"
  read -r line
done
exit 0

# 5) processa stdin estandart i mostra numerada linea a linea
num=1
while read -r line
do
  echo "$num: $line "
  ((num++))
done
exit 0

# 4) processar l'entrada estandart linea a linea
while read -r line
do 
  echo $line
done
exit 0

# 3) interar arguments amb shift
while [ -n "$1" ]
do
  echo "$1, $#, $*"
  shift
done

# 2) comptador decreixent del arg [N-0]
MIN=0
num=$1
while [ $num -ge $MIN ] 
do
  echo -n "$num, "
  ((num--)) 
done	
exit 0

# 1) mostrar un comptador del 1 a MAX
MAX=10
num=1
while [ $num -le $MAX ] 
do
  echo "$num"
  ((num++))
done	
exit 0

