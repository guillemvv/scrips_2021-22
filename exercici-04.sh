#! /bin/bash 
# @Guillem Vallespinos
# Curs 2021-2022
# Febrer 17
# Descripcio: Fer un programa que rep com a arguments numeros de mes i indica per cada 		     mes rebut quants dies te.
# -------------------------------------------------------------------------------------

ERR_ARGS=1

if ($# -le 1)
	echo "ERROR: arguments no valits"
	echo "util: $0 arg ..."
	exit $ERR_ARGS
fi

for arg in $*
do
	if [ $arg -lt "1" -o $arg -gt "12" ]; then
		echo "Error, el numero a de ser entre 1 i 12"
	else
		case $arg in 
			1|3|5|7|8|10|12)
				echo "$arg te 31 dies.";;
			2)
				echo "$arg te 28 dies ";;
			4|6|9|11)
				echo "$arg te 30dies";;
		esac
	fi
done
exit 0





