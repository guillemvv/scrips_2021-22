#! /bin/bash
# @Guillem Vallespinos
# Curs 2021-2022
# Febrer 2022
# exemples case
#----------------------------
#

case $1 in
  "dl"|"dt"|"dc"|"dj"|"dv")
    echo "$1 laborable"
    ;;
  "ds"|"dm")
    echo "$1 es festiu"
    ;;
  *)
    echo "$1 no es un dia"
esac
exit 0

#-------------------------------------

case $1 in
  [aeiou])
    echo "$1 son vocals"
  ;;
  [bcdfghjklmnpqrstvwxyz])
    echo "$1 es una consonant"
    ;;
  *)
    echo "$1 es una altre cosa"
esac
exit 0

#--------------------------------------

case $1 in
  "pere"|"pau"|"joan")
    echo "es un nen"
    ;;
  "marta"|"anna"|"julia")
    echo "es una nena"
    ;;
  *)
    echo "es indefinit"
    ;;
esac
exit 0






