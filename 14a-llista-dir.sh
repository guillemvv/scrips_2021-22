#! /bin/bash 
# @Guillem Vallespinos
# Curs 2021-2022
# (data)
# Descripcio: 
# ---------------------------------

ERR_ARG=1
ERR_DIR=2
dir=$1
# validar argument
if [ $# -ne 1 ]
then
  echo "ERROR: num args incorrecte"
  echo "usage: prog arg1"
  exit $ERR_ARG
fi

# validar directori
if ! [ -d $dir ]; then
  echo "ERROR: $dir no es un directori"
  echo "usage: $0 dir"
  exit $ERR_DIR
fi

# xixa
ls $dir
exit 0

