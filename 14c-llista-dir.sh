#! /bin/bash 
# @Guillem Vallespinos
# Curs 2021-2022
# (data)
# Descripcio: per cada alement de directori dir si es regular, link, dir o altres
# -------------------------------------------------------------------------------

ERR_ARG=1
ERR_DIR=2
dir=$1

# 1) validar argument
if [ $# -ne 1 ]
then
  echo "ERROR: num args incorrecte"
  echo "usage: prog arg1"
  exit $ERR_ARG
fi

# 2) validar directori
if ! [ -d $dir ]; then
  echo "ERROR: $dir no es un directori"
  echo "usage: $0 dir"
  exit $ERR_DIR
fi

# 3) xixa
llista_dir=$(ls $dir)
for nom in $llista_dir
do
  if [ -h "$dir/$nom" ]; then
    echo " "$dir/$nom" és link"
  elif [ -d "$dir/$nom" ]; then
    echo " "$dir/$nom" és un directori"
  elif [ -f "$dir/$nom" ]; then
    echo " "$dir/$nom" és un regular file"
  else
    echo " "$dir/$nom" és una altre cosa"
fi
done
exit 0

