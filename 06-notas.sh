#! /bin/bash
# @Guillem Vallespinos
# Curs 2021-2022
# Febrer 2022
# Validar nota: suspès, aprovat
# -------------------------------
ERR_ARGS=1
ERR_NOTA=2

# 1) si num args no es correcte plegar
if [ $# -ne 1 ]; then
  echo "Error numero arguments incorrecte"
  echo "Usage: $0 nota"
  exit $ERR_ARGS
fi

# 2) si nota no es [0-10] plegar
if ! [ $1 -ge 0 -a $1 -le 10 ]; then
  echo "Error nota $1 no valida"
  echo "nota pren valors de 0 a 10"
  echo "Usage: $0 nota"
  exit $ERR_NOTA
fi

# Xixa
nota=$1
if [ $nota -lt 5 ]; then
  echo "La nota $nota és un Suspes"
elif [ $nota -lt 7 ]; then
  echo "La nota $nota és un Aprovat"
elif [ $nota -lt 9 ]; then
  echo "La nota  $nota és un Notable"
else
  echo "La nota $nota és un Excel·lent"
fi
exit 0
