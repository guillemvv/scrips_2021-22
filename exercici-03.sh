#! /bin/bash 
# @Guillem Vallespinos
# Curs 2021-2022
# Febrer 17
# Descripcio: Fer un comptador des de zero fins al valor indicat per l'argument rebut
# ------------------------------------------------------------------------------------

MAX=$1
count=0

while [ $count -le $MAX ]
do
  echo "$count"
  ((count++))
done
exit 0






