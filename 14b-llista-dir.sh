#! /bin/bash 
# @Guillem Vallespinos
# Curs 2021-2022
# (data)
# Descripcio: numerar un a un els noms dels fitchers
# --------------------------------------------------

ERR_ARG=1
ERR_DIR=2
dir=$1

# 1) validar argument
if [ $# -ne 1 ]
then
  echo "ERROR: num args incorrecte"
  echo "usage: prog arg1"
  exit $ERR_ARG
fi

# 2) validar directori
if ! [ -d $dir ]; then
  echo "ERROR: $dir no es un directori"
  echo "usage: $0 dir"
  exit $ERR_DIR
fi

# 3) xixa
llista_dir=$(ls $dir)
num=1
for nom in $llista_dir
do
  echo "$num: $nom"
  ((num++))
done
exit 0

