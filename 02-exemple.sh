#! /bin/bash
# @Guillem Vallespinos
# 3 Fabrer 2022
# Exemple processamenr argumrnts
#--------------------------------------------------
#


# llista d'arguments
echo '$*: ' $*
# llists d'argumrnts "s'axpandeix en paraulas separadas"
echo '$@: ' $@
# numero d'arguments
echo '$#: ' $#
# nom del programa
echo '$0: ' $0
# argument 1
echo '$1: ' $1
# argument 2
echo '$2: ' $2
# argument 9
echo '$9: ' $9
# argument 1 mes un 0 al inal
echo '$10: ' $10
# argument 10
echo '${10}: ' ${10}
# argument 1 mes un 1 al final
echo '$11: ' $11
# argument 11
echo '${11}: ' ${11}
# proces que utilitza
echo '$$: ' $$

nom="puig"
echo "${nom}theworld"

exit 0
