#! /bin/bash
# @Guillem Vallespinos
# Curs 2021-2022
# 
##
# si no arg error
# si no es dir error
# si es un dir ---> llistar-lo 
#------------------------
#

ERR_ARG=1
ERR_NODIR=2

# 1 directori o ruta de directori valid
if [ $# -ne 1 ]; then
  echo "ERROR: num argument incorrecte"
  echo "usage: $0 dir"
  exit $ERR_ARG
fi

# si no es un directori
if ! [ -d $1 ]; then
  echo "ERROR: $1 no es un directori"
  echo "usage: $0 dir"
  exit $ERR_NODIR
fi

# xixa
dir=$1
ls $dir
exit 0
