#! /bin/bash 
# @Guillem Vallespinos
# Curs 2021-2022
# Febrer 17
# Descripcio: Mostra els arguments rebuts linea a linea, tot numerant-los
# ------------------------------------------------------------------------

num=1
arguments=$*

for arg in $arguments
do
  echo "$num: $arg"
  ((num++))
done
exit 0


while [ -n "$1" ]
do
  echo "$num: $1"
  shift
  ((num++))
done
exit 0
