#! /bin/bash
# @Guillem Vallespinos
# Curs 2021-2022
# Febrer 2022
#
#--------------------------
ERR_ARGS=1
ERR_FILE=2

# arguments
if [ $# -ne 1 ]; then
  echo "Error numero arguments incorrecte"
  echo "Usage: $0 file"
fi
exit $ERR_ARGS

# xixa
fit=$1
if [ ! -e $fit ]; then
  echo "$fit no existeis"
  exit $ERR_FILE
elif [ -f $fit ]; then
  echo "$fit és un regular file"
elif [ -h $fit ]; then
  echo "$fit és un directori"
elif [ -d $fit ]; then
  echo "$fit és un link"
else
  echo "$fit és una altre cosa"
fi
exit 0



