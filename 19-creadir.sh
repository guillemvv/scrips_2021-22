#! /bin/bash 
# @Guillem Vallespinos
# Curs 2021-2022
# (data)
# Descripcio: generar directoris. " 0 tot correcte, 1 error argumente, 2 error directoris" "mkdir no genera cap sortida"
# ---------------------------------

OK=0
ERR_ARG=1
ERR_DIR=2
status=0

if [ $# -lt 1 ]
then
	echo "ERROR: arguments no valids"
	echo "utilitza $0 nomdir [...]"
	echo $ERR_ARG
	exit $ERR_ARG
fi


for dir in $*
do
	mkdir -p $dir &> /dev/null
	if [ $? -ne 0 ]
	then
		echo "Error: no s'ha creat $dir" >&2
		status=$ERR_DIR
	fi
done
exit $status

