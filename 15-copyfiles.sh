#! /bin/bash 
# @Guillem Vallespinos
# Curs 2021-2022
# (data)
# Descripcio: 
# --------------------------------

ERR_ARG=1
ERR_FILE=2
ERR_DIR=3

# validar arguments
if [ $# -lt 2 ]
then
  echo "ERROR: num args incorrecte"
  echo "usage: $0 arg1 arg2"
  exit $ERR_ARG
fi

#file="carta.txt treball.txt noexist.pwd"
#desti="/tmp"
desti=$(echo $* | sed 's/^.* //')
llista_files=$(echo $* | sed 's/ [^ ]*$//')

# validar fitcher
if [ ! -f $file ]
then
  echo "$1 no existe"
  exit $ERR_FILE
fi

# validar desti
if [ ! -d $desti ]
then
  echo "$2 no es un desti valid"
  exit $ERR_DIR
fi

# programa
for fil in $file
do
  if [ ! -f $file ]
    then
  else
    cp $fil $desti
  fi
done
exit 0



