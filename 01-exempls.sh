#! /bim/bash
# @Guillem Vallespinos
# Febrer 2022
# Exemple de primer programa
# Normes:
#    shebang
#    capçalera: descripcio, data, autor
#-------------------------------------------
# es pot fer tot allo que es fa a la linia de comandes

echo "Hello World"
nom='pere pou prat'
edat=25
echo $nom $edat
echo -e "nom: $nom\n edat: $edat\n"
echo -e 'nom: $nom\n edat: $edat\n'
uname -a
uptime
echo $SHLVL
echo $SHELL
echo $((4*32))
echo $((edat*3))
exit 0
