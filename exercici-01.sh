#! /bin/bash 
# @Guillem Vallespinos
# Curs 2021-2022
# Febrer 17
# Descripcio: Mostrar l'entrada estandard numerant linea a linea
# --------------------------------------------------------------

num=1

while read -r line
do
  echo "$num: $line "
  ((num++))
done
exit 0



